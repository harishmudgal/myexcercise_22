use std::fs::{self, File};
use std::io::BufReader;
use std::io::Write;
use std::io::{self, BufRead};
use std::collections::HashSet;
extern crate clap;

use clap::{Arg, App};

struct Person {
    Fin: String,
    am: String,
}
fn main() {

    let matches = App::new("My Test Program")
    .version("0.1.0")
    .author("Hackerman Jones <hckrmnjones@hack.gov>")
    .about("Teaches argument parsing")
    .arg(Arg::with_name("file")
             .short("f")
             .long("file")
             .takes_value(true)
             .help("A cool file"))
    .arg(Arg::with_name("out")
             .short("o")
             .long("out")
             .takes_value(true)
             .help("Five less than your favorite number"))
    .get_matches();

    let myfile = matches.value_of("file").unwrap_or("input.txt");
    let ofile = matches.value_of("out").unwrap_or("output.txt");

    let f = File::open(myfile);

    let f3 = BufReader::new(f.expect("file name is incorrect"));
    // println!("{:?}",f3);
    // let f1 = OpenOptions::new()
    //             .read(true)
    //             .write(true)
    //             .create(true)
    //             .open("src/output.txt");
    let mut f1 = File::create(ofile).expect("unable to create");
    let mut cnt = 0;
    let mut ctrl = 0;
    let mut ctrl2 = 0;
    // let mut nam: Vec<String> = Vec::new();
     let mut amt = HashSet::new();
    for line in f3.lines() {
        ctrl2 = 0;
            if amt.contains(&cnt) {
                ctrl2 = 1;
            }
        if ctrl2 == 1 {
            cnt = cnt + 1;
            continue;
        }
         let mut tk1 = 0;
        let s = line.expect("N");
        let tokens: Vec<&str> = s.split(",").collect();
       // println!("{:?}", tokens[1]);
        //  let person = Person { Fin: String::from(tokens[0]),
        //     amt: String::from(tokens[2]),
        //     };
        let f = File::open(myfile);

        let f3 = BufReader::new(f.expect("file name is incorrect1"));

       // let mut tk: i32 = tokens[2].parse().unwrap();

        for line2 in f3.lines() {
            // if cnt == ctrl {
            //     continue;
            // }
            //println!("{:?}", cnt);
            let s1 = line2.expect("N");
            let tokens2: Vec<&str> = s1.split(",").collect();
           // println!("{:?}", tokens2[1]);
            let tk2: i32 = tokens2[2].parse().unwrap();
            if tokens[0] == tokens2[0] {
              //  println!("sub loop");
               tk1 = tk1 + tk2;
               amt.insert(ctrl);
               // println!("{:?}", amt.push(ctrl));
                // write!(f1, "{},{}", tokens[0], tk).expect("cannot write");
                // ctrl = 1;
            }
            ctrl = ctrl + 1;
           // println!("{}", tk1);
        }
        cnt = cnt + 1;
       ctrl=0;
       let person = Person {Fin:String::from(tokens[0]),
    am:String::from(tk1.to_string())};
      //  println!("{}", tk1);
        writeln!(f1, "{},{}", person.Fin, person.am).expect("cannot write");
    }
}
